'use strict';

const AWS = require('aws-sdk');
const Chance = require('chance');

exports.handler = (event, context, callback) => {

    var chance = new Chance();

    const sns = new AWS.SNS({
        region: 'eu-west-1',
        apiVersion: '2010-03-31'
    });

    console.log("Topic ARN:", process.env.TOPIC_ARN)

    const sessionId =  chance.guid()

    sns.publish({
        Message: JSON.stringify({
            sessionId: sessionId,
            route: 'Customer',
            history: 'Experience',
            name: chance.name(),
            age: chance.age(),
            email: chance.email(),
            address: chance.address(),
            income: chance.euro({max: 2500})
    }),
        TopicArn: process.env.TOPIC_ARN
    }, function (err, data) {
        if (err) {
            console.log(err.stack);
            callback("error");
        }
        console.log('push sent');
        console.log(data);
    });

    const response = {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*', // Required for CORS support to work
        },
        body: JSON.stringify({
            sessionId: sessionId
        }),
    };

    callback(null, response);
};

