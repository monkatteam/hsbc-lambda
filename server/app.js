var app = require('express')();
var cors = require('cors');
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(cors());

io.set('origins', '*:*');

var server = app.listen(3030, () => {
    console.log("app running on port.", server.address().port);
});

let sockets = [];

app.get('/', (req, res) => {
    res.status(200).send("Socket end point");
    sockets.forEach((socket) => {
        // if (socket.dynamoId === [the id sent with the call from the lambda]){
            console.log('sending data to socket' + iterator);
            socket.emit('message', 'data has been received from the lambda');
        // }
    })
});

io.on('connection', socket => {
    console.log(`a user connected. ID:  ${socket.handshake.query.dynamoId}`);

    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
    socket.dynamoId = socket.handshake.query.dynamoId;
    sockets.push(socket);
});

http.listen(4040, () => {
    console.log('listening on *:4040');
});
