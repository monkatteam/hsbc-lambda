'use strict';

const update = require('lodash/update')
const AWS = require('aws-sdk');
var sleep = require('system-sleep');

exports.handler = (event, context, callback) => {


    var message = JSON.parse(event.Records[0].Sns.Message);
    console.log('Message received from SNS:', message);

    if (message.route != process.env.ID) {
        console.log("Request for ", message.route, ' was ignored.')
        callback(null, "Ignored.");
    } else {
        console.log("sleeping for ", process.env.DELAY)
        sleep(process.env.DELAY);
        console.log("Continuing..")
        update(message, 'history', function (n) {
            return n + ',' + process.env.ID
        });
        message.route = process.env.NEXT;
        console.log('New message:', message);

        const sns = new AWS.SNS({
            region: 'eu-west-1',
            apiVersion: '2010-03-31'
        });

        sns.publish({
            Message: JSON.stringify(message),
            TopicArn: process.env.TOPIC_ARN
        }, function (err, data) {
            if (err) {
                console.log(err.stack);
                callback("error");
            }
            console.log('push sent');
            console.log(data);
        });

        callback(null, "Success");
    }


};
