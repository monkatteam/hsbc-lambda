aws s3 sync . s3://hsbc-serverless --profile hsbc --acl public-read
aws s3 website s3://hsbc-serverless --index-document index.html --profile hsbc