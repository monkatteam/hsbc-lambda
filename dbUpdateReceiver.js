'use strict';

const AWS = require('aws-sdk');
const http = require('http');

exports.handler = (event, context, callback) => {

  console.log('Received an update!', JSON.stringify(event));
  const record = event.Records[0];
  console.log('SessionId: ', record.dynamodb.NewImage.sessionId.S);
  console.log('History: ', record.dynamodb.NewImage.history.S);

  const postData = querystring.stringify({
    sessionId:  record.dynamodb.NewImage.sessionId.S,
    history: record.dynamodb.NewImage.history.S,
  });

  const options = {
    hostname: 'localhost',
    port: 3000,
    path: '/',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': Buffer.byteLength(postData)
    }
  };

  const req = http.request(options, (res) => {
    console.log(`STATUS: ${res.statusCode}`);
    console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      console.log(`BODY: ${chunk}`);
    });
    res.on('end', () => {
      console.log('No more data in response.');
      context.done(null);
    });
  });

  req.on('error', (e) => {
    console.error(`problem with request: ${e.message}`);
    context.done("Failed");
  });

  // write data to request body
  req.write(postData);
  req.end();
};

