import React, { Component } from 'react';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import io from 'socket.io-client';
import LinearProgress from 'material-ui/LinearProgress';
import randomstring from 'randomstring';

const gifs = {
  Submitted: 'https://media.giphy.com/media/l0Iyn34fotpL8K9wI/giphy.gif',
  Started: 'https://media.giphy.com/media/l0Iyn34fotpL8K9wI/giphy.gif',
  Customer: 'https://media.giphy.com/media/l0Iyn34fotpL8K9wI/giphy.gif',
  Checks: 'https://media.giphy.com/media/l0Iyn34fotpL8K9wI/giphy.gif',
  Decision: 'https://media.giphy.com/media/l0Iyn34fotpL8K9wI/giphy.gif',
  Account: 'https://media.giphy.com/media/l0Iyn34fotpL8K9wI/giphy.gif',
  Done: 'https://media.giphy.com/media/111ebonMs90YLu/giphy.gif',
}

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
        route: '',
        progress: 0,
        sessionId: '',
    };
    this.submit = this.submit.bind(this);
  }

  setupSocket(sessionId) {
      const socket = io(`http://localhost:4040?dynamoId=${sessionId}`);

      socket.on('message', function(msg){
          console.log('MESSAGE RECEIVED', msg)
      });
  }

  submit() {
    this.setState({route: 'Submitted'});
    fetch('https://tduo2wy9q4.execute-api.eu-west-1.amazonaws.com/dev/application')
      .then(resp => resp.json())
      .then(json => {
        this.setState({sessionId: json.sessionId, progress: 10, route: 'Started'});
        console.log(json)
        this.poll();
        this.setupSocket(json.sessionId);
      });
  }

  poll(){
    this.getStatus();
  }

  getStatus(){
    fetch('https://tduo2wy9q4.execute-api.eu-west-1.amazonaws.com/dev/statusCheck', {
      headers: {
        'user-agent': 'Mozilla/4.0 MDN Example',
        'content-type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify({sessionId: this.state.sessionId}),
      }
    )
      .then(resp => resp.json())
      .then(({Item: {route}}) => {
        this.setState({progress: this.state.progress + 5, route});
        if (route !== 'Done'){
          this.getStatus();
        } else {
          this.setState({progress: 100});
        }
      });
  }

  render() {
    return (
      <MuiThemeProvider>
        <div className="App">
          <header className="App-header">
            <h1 className="App-title">HSBC Serverless</h1>
            <img src="https://www.geniuswithin.co.uk/wp-content/uploads/2014/02/hsbc-logo-square-130x130.gif"/>
          </header>
          <div style={{marginTop: '15px'}}>
            <RaisedButton onClick={this.submit}>Submit application</RaisedButton>
          </div>
          {this.state.route !== '' ? (<div><h1>{this.state.route}</h1> </div>) : (<div></div>)}
          {this.state.progress > 0 && <LinearProgress variant="determinate" mode="determinate" value={this.state.progress} />}
          <br/>
          {this.state.progress > 0 && <img src={gifs[this.state.route]} style={{maxWidth: '30%'}} />}
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
