'use strict';

const AWS = require('aws-sdk');

exports.handler = (event, context, callback) => {

  const sessionId = JSON.parse(event.body).sessionId
  console.log("Incoming sessionId:", sessionId);

  AWS.config.apiVersions = {
    dynamodb: '2012-08-10',
  };

  const documentClient = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName :process.env.dbtable,
    AttributesToGet: [
      "route",
      "history",
    ],
    Key : {
      sessionId : sessionId,
    }
  };

  console.log(params);

  documentClient.get(params, function(err, data){
    if (err) console.log(err, err.stack);
    console.log("returning :" +data)

    const response = {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
      },
      body: JSON.stringify(data),
    };

    callback(null, response);

  })

};

