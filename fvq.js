'use strict';

const AWS = require('aws-sdk');

exports.handler = (event, context, callback) => {
    var message = event.Records[0].Sns.Message;
    console.log('Message received from SNS:', message);

    AWS.config.apiVersions = {
        dynamodb: '2012-08-10',
    };

    var documentClient = new AWS.DynamoDB.DocumentClient();

    var params = {
        TableName :process.env.dbtable,
        Item: JSON.parse(message)
    };

    console.log(params)

    documentClient.put(params, function(err, data){
        if (err) console.log(err, err.stack);
        console.log("stored :" +data)
    })

    callback(null, "Success");
};
